<?php
require_once './vendor/autoload.php';


use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

Class User{

    protected $database;
    protected $dbname ="users";

    public function __construct(){
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/secret/fir-php-e1c02-013823e163be.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
        $this->database = $firebase->getDatabase();
    }

    public function get(int $userId=null){
        if(empty($userId)|| !isset($userId)){
            return false;
        }
        if($this->database->getReference($this->dbname)->getSnapshot()->hasChild($userId)){
            return $this->database->getReference($this->dbname)->getChild($userId)->getValue();
        }
    }

    public function insert(array $data){
        if(empty($data) || !isset($data)){
            return false;
        }
        foreach($data as $key => $val){
            $this->database->getReference()->getChild($this->dbname)->getChild($key)->set($val);
        }
        return true;
    }

    public function remove(int $userId=null){
        if(empty($userId)|| !isset($userId)){
            return false;
        }
        if($this->database->getReference($this->dbname)->getsnapshot()->hasChild($userId)){
            $this->database->getReference($this->dbname)->getChild($userId)->remove();
            return true;
        }
        else{
            return false;
        }
    }

} 
$users  = new User();
//  Insert the data
// var_dump($users->insert([
//     '4'=>"karan",
//     '2'=>"tasdarun",
//     '3'=>"sunny"
// ]));
// Get Data
//  var_dump($users->get(1));
// Remove Data 
// var_dump($users->remove(4));

